console.log('Starting notes.js...')

const fs = require('fs')

let fetchNotes = () => {
  if (!fs.existsSync('notes-data.json')) return []
  let notesString = fs.readFileSync('notes-data.json')
  return JSON.parse(notesString)
}

let saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes))
}

let addNote = (title, body) => {
  let notes = fetchNotes()
  let note = {
    title,
    body
  }

  let duplicateNotes = notes.filter((note) => note.title === title)

  if (duplicateNotes.length !== 0) return `ERROR - Duplicate title: ${note.title}`
  notes.push(note)
  saveNotes(notes)
  return `Note successfully saved:\n${note.title}\n${note.body}`
}

let getAll = () => {
  let notes = fetchNotes()
  if (notes.length === 0) return 'ERROR - There are 0 notes in DB'
  notes = notes.map((note, index) => `${index + 1}.\nTitle: ${note.title}\nBody:\n${note.body}\n\n`).join('')
  return notes
}

let getNote = (title) => {
  let notes = fetchNotes()
  let note = notes.filter((note) => note.title === title)
  if (notes.length === 0) return 'ERROR - There are 0 notes in DB'
  if (note.length === 0) return `ERROR - Note not found by title:\n${title}`
  return `Note found.\n\nTitle:\n${note[0].title}\n\nBody:\n${note[0].body}`
}

let removeNote = (title) => {
  let notes = fetchNotes()
  if (notes.length === 0) return 'ERROR - There are 0 notes in DB'
  let filteredNotes = notes.filter((note) => note.title !== title)
  if (filteredNotes.length === notes.length) return `ERROR - Note not found by title:\n${title}`
  saveNotes(filteredNotes)
  return 'Note deleted successfully'
}

module.exports = {
  addNote, // it is the same as addNote: addNote
  getAll,
  getNote,
  removeNote
}

// PREVIOUS GRADUAL STEPS
// module.exports.age = 33

// module.exports.addNote = () => {
//   console.log('addNote')
//   return 'New note'
// }

// module.exports.add = (a, b) => {
//   return a + b
// }
