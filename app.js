console.log('Starting app.js...')

const fs = require('fs')
const _ = require('lodash')
const yargs = require('yargs')

const notes = require('./notes.js')

const requiredArgs = {
  title: {
    describe: 'Title of the note',
    demand: true,
    alias: 't'
  },
  body: {
    describe: 'Body of the note',
    demand: true,
    alias: 'b'
  }
}

const argv = yargs
  .command('add', 'Add a new note', {
    title: requiredArgs.title,
    body: requiredArgs.body
  })
  .command('list', 'List all notes')
  .command('read', 'Fetch a note', {
    title: requiredArgs.title
  })
  .command('remove', 'Delete a note', {
    title: requiredArgs.title
  })
  .help()
  .argv
// let command = process.argv[2] // process is like document in a browser; argv is ArgumentsVector
let command = argv._[0]
// console.log(`Command: ${command}`)
console.log('Process:', process.argv)
console.log('Yargs:', argv)

if (command === 'add') {
  console.log(notes.addNote(argv.title, argv.body))
} else if (command === 'list') {
  console.log(notes.getAll())
} else if (command === 'read') {
  console.log(notes.getNote(argv.title))
} else if (command === 'remove') {
  console.log(notes.removeNote(argv.title))
} else {
  console.log('Command not recognized')
}

// PREVIOUS GRADUAL STEPS
// const os = require('os')

// let filteredArray = _.uniq(['Malac', 1, 'Malac', 1, 2, 3, 4, 5])
// console.log(filteredArray)

// console.log(_.isString(true))
// console.log(_.isString('Zoli'))
// console.log(_.isString(3))

// console.log(notes.add(3, 9))

// let res = notes.addNote()
//
// console.log(res)

// let user = os.userInfo()
//
// fs.appendFile('greeting.txt', `Hello ${user.username}! You are ${notes.age}.`, function (err) {
//   if (err) console.log('Unable to write file')
// })
