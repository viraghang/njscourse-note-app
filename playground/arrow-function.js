let square = x => x * x // if there is only 1 arg
console.log(square(9))

let user = {
  name: 'Zoli',
  sayHi: () => {
    console.log(arguments) // global argument object
    console.log(`Hi. I'm ${this.name}`) // Hi. I'm undefined
  },
  sayHiAlt () {
    console.log(arguments) // current-scope argument object
    console.log(`Hi. I'm ${this.name}`) // Hi. I'm Zoli
  }
}

user.sayHi(1, 2, 3)
